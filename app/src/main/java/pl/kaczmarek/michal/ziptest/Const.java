package pl.kaczmarek.michal.ziptest;

public class Const {
	public static final String PREFIX = "http://publib.boulder.ibm.com/";
	public static final String SUFFIX = "bpcsamp/monitoring/clipsAndTacks/download/ClipsAndTacksF1ForModeler.zip";
	public static final String FULL = PREFIX + SUFFIX;
	public static final String MIME_TYPE = "application/zip";
}
