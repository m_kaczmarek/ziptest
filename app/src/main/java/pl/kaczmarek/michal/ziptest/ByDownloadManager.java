package pl.kaczmarek.michal.ziptest;

import android.app.DownloadManager;
import android.content.Context;
import android.net.Uri;
import android.os.Environment;

public class ByDownloadManager {

    private final Context context;

    public ByDownloadManager(Context context) {
        this.context = context;
    }

    public void getIt() {
        DownloadManager downloadManager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(Const.FULL))
                .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
                .setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "/zips/example.zip")
                .setMimeType(Const.MIME_TYPE);
        request.allowScanningByMediaScanner();
        downloadManager.enqueue(request);
    }
}
