package pl.kaczmarek.michal.ziptest;

import android.app.DownloadManager;
import android.content.Context;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class Saver {


    private static final String TAG = Saver.class.getSimpleName();

    public void writeResponseBodyToDisk(Context context, byte[] bytes, String name) throws IOException {
        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), name);
        OutputStream outputStream = null;
        try {
            outputStream = new FileOutputStream(file);
            outputStream.write(bytes, 0, bytes.length);
            outputStream.flush();
        } finally {
            if (outputStream != null) {
                outputStream.close();
            }
        }

        DownloadManager manager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
        if (null != manager) {
            long id = manager.addCompletedDownload(name, name, true, Const.MIME_TYPE, file.getAbsolutePath(), file.length(), true);
            Log.d(TAG, "Save file with id: " + id);
        }
    }
}
