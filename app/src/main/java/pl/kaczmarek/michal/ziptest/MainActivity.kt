package pl.kaczmarek.michal.ziptest

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Button

class MainActivity : AppCompatActivity() {

    lateinit var downloadManager: Button
    lateinit var retrofit:Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        downloadManager = findViewById(R.id.download_manger)
        downloadManager.setOnClickListener {
            val manager = ByDownloadManager(this)
            manager.getIt()
        }

        retrofit = findViewById(R.id.retrofit)
        retrofit.setOnClickListener {
            val byRetrofit = ByRetrofit(this)
            byRetrofit.haveFun()
        }
    }
}
