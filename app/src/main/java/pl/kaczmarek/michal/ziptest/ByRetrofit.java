package pl.kaczmarek.michal.ziptest;

import android.content.Context;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.http.GET;

public class ByRetrofit {

    private final Context context;
    private final Retrofit retrofit;

    public ByRetrofit(Context context) {
        this.context = context;
        final HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        final OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .build();
        retrofit = new Retrofit.Builder()
                .baseUrl(Const.PREFIX)
                .client(client)
                .build();
    }

    public void haveFun() {
        retrofit.create(Api.class)
                .get()
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        final Saver saver = new Saver();
                        try {
                            saver.writeResponseBodyToDisk(context, response.body().bytes(), "retrofit.zip");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
    }

    private interface Api {

        @GET(Const.SUFFIX)
        Call<ResponseBody> get();
    }
}
